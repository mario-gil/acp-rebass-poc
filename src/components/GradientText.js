import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Text } from 'rebass';
import { style, display } from 'styled-system';

/*
** This is how you would make a custom style
** that uses values from the global theme
*/
const webkitBackgroundClip = style({
  prop: 'backgroundClip',
  cssProperty: '-webkit-background-clip',
  key: 'backgroundClip',
});

/*
** Rebass components are styled-components
** so they can be modified as one
*/
const GradientText = styled(Text)`
background: #1e5799; /* Old browsers */
background: -moz-linear-gradient(left, #1e5799 0%, #207cca 0%, #e82ef2 100%, #7db9e8 100%);
background: -webkit-linear-gradient(left, #1e5799 0%,#207cca 0%,#e82ef2 100%,#7db9e8 100%);
background: linear-gradient(to right, #1e5799 0%,#207cca 0%,#e82ef2 100%,#7db9e8 100%);
${display}
${webkitBackgroundClip}
`;

GradientText.propTypes = {
  backgroundClip: PropTypes.string,
};

/*
** This is how we generate our own variations
** starting from Rebass components
*/
export default props => (
  <GradientText
    blacklist="backgroundClip" // Needed to avoid passing this prop to dom thus generating a warning
    color="transparent"
    backgroundClip="text"
    display="inline-block"
    {...props}
  />
);
