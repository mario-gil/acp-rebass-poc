import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'rebass';
import { display, style } from 'styled-system';
import styled from 'styled-components';
import tag from 'clean-tag';

const maxHeight = style({
  prop: 'maxHeight',
  cssProperty: 'max-height'
});

const boxSizing = style({
  prop: 'boxSizing',
  cssProperty: 'box-sizing',
  key: 'boxSizing',
});

const DefaultInput = styled(tag)`
${boxSizing}
${display}
${maxHeight}
`;

DefaultInput.displayName = "DefaultInput";
DefaultInput.propTypes = {
  boxSizing: PropTypes.string,
  display: PropTypes.string,
  maxHeight: PropTypes.string,
}
DefaultInput.defaultProps = {
  blacklist: [...Object.keys(Input.propTypes)],
  boxSizing: "content",
  display: "flex",
  maxHeight: "1em",
}

const fullBlacklist = Object.keys(DefaultInput.propTypes).concat(Object.keys(DefaultInput.defaultProps));

export default props => (
  <DefaultInput
    is={Input}
    blacklist={fullBlacklist}
    {...props}
  />
);
