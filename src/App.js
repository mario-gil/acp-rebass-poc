import React, { Component } from 'react';
import { Provider, Heading, Button, Box, ButtonOutline, Flex } from 'rebass';
import theme from './theme';
import GradientText from './components/GradientText';
import DefaultInput from './components/DefaultInput';

class App extends Component {
  render() {
    return (
      <Provider theme={theme}>
        <Box px={4} py={4} color='white'>
          <GradientText
            is='h1'
            fontSize={[4, 5, 6]} // This is a responsive style
          >
            Hello, Rebass
          </GradientText>
        </Box>
        <Flex
          px={4}
          py={4}
          justifyContent='space-between'
          flexDirection={['column', 'row']}
        >
          <Flex>
            <Heading fontSize={[2, 3, 4]} pr={2} color='blue'>
              Enter your name
            </Heading>
          </Flex>
          <Flex alignItems="center" flex={1} mb={[2, 0]}>
            <DefaultInput placeholder="Default input" />
          </Flex>
          <Flex ml={[0, 2]} justifyContent="center" alignItems="center">
            <Button>Beep</Button>
            <ButtonOutline ml={2}>Boop</ButtonOutline>
          </Flex>
        </Flex>
      </Provider>
    );
  }
}

export default App;
