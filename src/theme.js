const textFillColors = ['transparent'];
const backgroundClip = {
  text: 'text',
};
const boxSizing = {
  content: 'content-box',
}

const theme = {
  backgroundClip,
  boxSizing,
  textFillColors,
};

export default theme;
