# Design System POC - [Rebass](http://jxnblk.com/rebass/)

This demonstrates Rebass' composition capabilities while making use of
some libraries in which Rebass itself is built upon

## Reasoning

While `styled-components` is a convenient way of styling react apps sometimes making bulk changes is not so easy, also it still needs CSS to be written. As frontend engineers we always should strive to build robust design systems when dealing with apps bigger than a toy. Building the utilities needed to kickstart a design system can be cumbersome, that's why these support libraries suit us well.

Rebass' author has explained his approach in [this post](http://jxnblk.com/writing/posts/patterns-for-style-composition-in-react/). It's also a good read about React's composition patterns.

### Pros

- Enable easy component composability
- Kickstart a design system fast
- Expose a simple but powerful theme customization API
- Components are `styled-components` at their core

### Cons

- Issue with `styled-components` not exposing its prop whitelist so a workaround has to be used to avoid `unknown prop` warning in React

## Important files in this POC

This is really simple so by looking at `GradientText.js` and `DefaultInput.js` you should get a hold of how to perform component variations and the general taste of the library.
